import React from 'react';
import './style.css';
import { Link } from 'react-router-dom';

const Card = ({name, url}) =>
{
  //console.log(name)
  return(<Link to={{
        name: name,
        pathname: '/pokemon',
        url: url
      }}>
      <div className ="wrapper">
        <h1 style={styleP}>{name}</h1>
      </div>
    </Link>
  )
}

const styleP = 
{
  fontSize: 20,
  fontWeight: 'bold'
}

export default Card;