import React from 'react';
import './style.css';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';

class Pokemon extends React.Component
{
  constructor(props){
    super(props);
    this.state =
    {
      loading: true,
      url: '',
      pokemon: ''
    }  
  }

  componentDidMount()
  {
    const {location:{url}} = this.props;
    if(url)
    {
      fetch(url)
        .then(res => res.json())
        .then
        (
          (pokemon) =>
          {
            this.setState({loading: false, pokemon, url})
          },
          (error) =>
          {
            console.log(error);
            this.setState({loading: false, error, url})
          }           
        )
    }else
    {
      this.setState({url:null,loading:false})
    }
  }

  render()
  {
    const {loading, pokemon, url} = this.state;
    if(!loading && url === null) return <Redirect to={'/'}/>
    return(<React.Fragment>
      {
        loading ? <p>Esperando Pokemon...</p>:
        <div>
          <Link to={'/'}>
            <button>Volver a la lista</button>
          </Link>
          <h1>Nombre: {pokemon.name} <b> # {pokemon.order}</b></h1>
          <h2>Tipo: {pokemon.types.map(({type}) => (`${type.name} `))}</h2>
          
          <p>Habilidades:</p>
          <ul>
            {
              //console.log(pokemon),
              pokemon.abilities.map((item, key) => (
              <li key={key}>{item.ability.name}</li>
              ))
            }
          </ul>
        </div>
      }
    </React.Fragment>)
  }
}

export default Pokemon;