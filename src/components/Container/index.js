import React from 'react';
import './style.css';
import Card from '../Card';

class Container extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loading: true,
      error:'',
      results: []
    }
  }

  componentDidMount()
  {
    fetch ('https://pokeapi.co/api/v2/pokemon?limit=20')
    .then(res => res.json())
    .then(
      ({results}) => {
        //console.log(results);
        this.setState({loading:false, results})
      },
      (error) => 
      {
        console.log(error);
        this.setState({loading:false, error})
      }
    )
  }

  
  render()
  {
    const {loading, results} = this.state;
    if(loading) return <p>Cargando...</p>
    return(
      <div className="container">
        {
          results.map((pokemon, index) => (
            //console.log(pokemon.name),
            <Card key={index} name={pokemon.name} url={pokemon.url}/>
          ))
        }
      </div>
    );
  }

}

export default Container;