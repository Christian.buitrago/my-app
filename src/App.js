import React from 'react';
import Container from './components/Container'; 
import Pokemon from './components/Pokemon';
import { Route } from 'react-router-dom';

const App = () =>
<div>
    <Route path="/" exact component={Container} />
    <Route path="/pokemon" component={Pokemon} />
</div>

export default App;